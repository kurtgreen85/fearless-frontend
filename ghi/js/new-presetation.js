window.addEventListener("DOMContentLoaded", async () => {

    const url = "http://localhost:8000/api/conferences/";
  
    const response = await fetch(url);
  
    if (response.ok) {
      const data = await response.json();
  
      let selectTag = document.getElementById("conference");
      for (let conference of data.conferences) {
        let option = document.createElement("option");
        option.value = conference.abbreviation;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }
    const formTag = document.getElementById("create-presentation-form");
  formTag.addEventListener("submit", async (event) => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    let select = document.getElementById("conference")

    const locationURL = "http://localhost:8000/api/conferences/" + select.selectedIndex + "/presentations/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationURL, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newLocation = await response.json();
    }
  });
});